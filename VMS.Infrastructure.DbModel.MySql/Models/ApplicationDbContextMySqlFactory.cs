﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using VMS.Infrastructure.DbModel.Models;

namespace VMS.Infrastructure.DbModel.MySql
{
	public class ApplicationDbContextMySqlFactory : IDesignTimeDbContextFactory<ApplicationDbContextMySql>
	{
		public ApplicationDbContextMySql CreateDbContext(string[] args)
		{
			var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContextMySql>()
				.UseMySQL("server=localhost;UserId=root;Password=123456;database=mytestdb;"); //TODO: read connection string from config file
			return new ApplicationDbContextMySql(optionsBuilder.Options);
		}
	}
}
