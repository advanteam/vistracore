//using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using VMS.Infrastructure.DbModel.Models;
using VMS.Infrastructure.DbModel.Models.Analytics;
using VMS.Infrastructure.DbModel.Models.Geometry;
using VMS.Infrastructure.DbModel.Models.Identity;
using VMS.Infrastructure.DbModel.Models.Job;
using VMS.Infrastructure.DbModel.Models.Training;
using VMS.Infrastructure.DbModel.Models.Users;

namespace VMS.Infrastructure.DbModel.MySql {
    public class ApplicationDbContextMySql : ApplicationDbContext {
	    public ApplicationDbContextMySql()
	    {
	    }

	    public ApplicationDbContextMySql(DbContextOptions<ApplicationDbContextMySql> options)
			: base(options) { }

		//protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		//{
		//	optionsBuilder.UseMySQL("server=localhost;UserId=root;Password=123456;database=mytestdb;");
		//}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
	    {
		    base.OnModelCreating(modelBuilder);

			//modelBuilder.Entity<UserEntity>(entity => entity.Property(m => m.Id).HasMaxLength(85));
			//modelBuilder.Entity<UserEntity>(entity => entity.Property(m => m.NormalizedEmail).HasMaxLength(85));
			//modelBuilder.Entity<UserEntity>(entity => entity.Property(m => m.NormalizedUserName).HasMaxLength(85));
			//modelBuilder.Entity<IdentityRole>(entity => entity.Property(m => m.Id).HasMaxLength(85));
			//modelBuilder.Entity<IdentityRole>(entity => entity.Property(m => m.NormalizedName).HasMaxLength(85));
			//modelBuilder.Entity<IdentityUserLogin<int>>(entity => entity.Property(m => m.UserId).HasMaxLength(85));
			//modelBuilder.Entity<IdentityUserRole<int>>(entity => entity.Property(m => m.UserId).HasMaxLength(85));
			//modelBuilder.Entity<IdentityUserRole<int>>(entity => entity.Property(m => m.RoleId).HasMaxLength(85));
			//modelBuilder.Entity<IdentityUserToken<int>>(entity => entity.Property(m => m.UserId).HasMaxLength(85));
		    //modelBuilder.Entity<IdentityUserToken<int>>(entity => entity.Property(m => m.Name).HasMaxLength(85));
		    //modelBuilder.Entity<IdentityUserClaim<int>>(entity => entity.Property(m => m.Id).HasMaxLength(85));
		    //modelBuilder.Entity<IdentityUserClaim<int>>(entity => entity.Property(m => m.UserId).HasMaxLength(85));
		    //modelBuilder.Entity<IdentityRoleClaim<int>>(entity => entity.Property(m => m.Id).HasMaxLength(85));
		    //modelBuilder.Entity<IdentityRoleClaim<int>>(entity => entity.Property(m => m.RoleId).HasMaxLength(85));
			modelBuilder.Entity<ApplicationUserLogin>(entity => entity.Property(m => m.LoginProvider)
				.HasMaxLength(85));
			modelBuilder.Entity<ApplicationUserLogin>(entity => entity.Property(m => m.ProviderKey)
				.HasMaxLength(85));
			modelBuilder.Entity<ApplicationUserToken>(entity => entity.Property(m => m.LoginProvider)
				.HasMaxLength(85));
			
		}


	}
}