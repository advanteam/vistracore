﻿namespace VMS.Infrastructure.DbModel.Models.Users {
    public enum GroupStatus {
        None,
        Added,
        Removed
    }
}