namespace VMS.Infrastructure.DbModel.Models.Users {
	public class Trainer {
		public int ID { get; set; }
		public int ApplicationUserID { get; set; }
		public int GroupID { get; set; }
		public virtual ApplicationUser ApplicationUser { get; set; }
		public virtual Group Group { get; set; }
	}
}