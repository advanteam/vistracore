﻿namespace VMS.Infrastructure.DbModel.Models.Users {
    public enum UserRoles {
        Root,
        Admin,
        Trainee
    }

    public static class RoleConstants {
        public const string Root = "Root";
        public const string Admin = "Admin";
        public const string Trainee = "Trainee";

        public const string RootAdmin = "Root, Admin";
        public const string AdminTrainee = "Admin, Trainee";
    }
}