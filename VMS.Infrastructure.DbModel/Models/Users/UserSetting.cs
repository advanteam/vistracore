﻿namespace VMS.Infrastructure.DbModel.Models.Users {
    public class UserSetting {
        public int ID { get; set; }

        public bool ShowWelcomePage { get; set; }

        public int ApplicationUserID { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}