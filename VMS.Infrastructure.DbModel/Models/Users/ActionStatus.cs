﻿namespace VMS.Infrastructure.DbModel.Models.Users {
    public enum ActionStatus {
        None,
        Delete,
        Add,
        Edit
    }
}