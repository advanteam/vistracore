namespace VMS.Infrastructure.DbModel.Models.Users {
    public class Enrollment {
        public int ID { get; set; }
        public int ApplicationUserID { get; set; }
        public int GroupID { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Group Group { get; set; }

	    #region Specifications

	    /*public static Specification<Enrollment> ForGroup(Specification<Group> groupSpec) {
		    return Spec.MemberSpec((Enrollment e) => e.Group, groupSpec);
	    }

	    public static Specification<Enrollment> ForUser(Specification<ApplicationUser> userSpec) {
		    return Spec.MemberSpec((Enrollment e) => e.ApplicationUser, userSpec);
	    }*/

	    #endregion
    }
}