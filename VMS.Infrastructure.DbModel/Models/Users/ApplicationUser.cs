using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using LinqSpecs.NetStandard;
using Microsoft.AspNetCore.Identity;
using VMS.Infrastructure.DbModel.Models.Identity;

namespace VMS.Infrastructure.DbModel.Models.Users {
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser<int>{//<int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>, IUser<int> {
        public ApplicationUser() {
            Active = true;
        }

        //public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager) {
        //    // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
        //    ClaimsIdentity userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
        //    // Add custom user claims here
        //    return userIdentity;
        //}

        //[Required]
        public string FirstName { get; set; }

        public string LastName { get; set; }
        public string Address { get; set; }

        [DefaultValue(true)] public bool Active { get; set; }

        // Sealed user cannot be deleted
        public bool Sealed { get; set; }

        public int? OwnerId { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
	    public virtual ICollection<Trainer> Trainers { get; set; }

		public string FullName {
            get { return FirstName + " " + LastName; }
        }

        public DateTime DateCreated { get; set; }

        public string VisiblePassword { get; set; }

        //public ApplicationRole FirstRole {
        //    get { return Enumerable.First<ApplicationUserRole>(this.Roles).Role; }
        //}

        public virtual ApplicationUser Owner { get; set; }

	    #region Specifications

	    public static Specification<ApplicationUser> ById(int id)
	    {
			return new AdHocSpecification<ApplicationUser>(user => user.Id == id);
	    }
	    public static Specification<ApplicationUser> NameContains(string substring)
	    {
		    return new AdHocSpecification<ApplicationUser>(user => user.UserName.Contains(substring));
		}
	    public static Specification<ApplicationUser> IsActive(bool value)
	    {
		    return new AdHocSpecification<ApplicationUser>(user => user.Active);
	    }

	    #endregion
	}
}