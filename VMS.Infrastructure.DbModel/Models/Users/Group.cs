using System;
using System.Collections.Generic;
using System.ComponentModel;
using VMS.Infrastructure.DbModel.Models.Job;

namespace VMS.Infrastructure.DbModel.Models.Users {
    public class Group {
        public Group() {
            Active = true;
            DateCreated = DateTime.Now;
        }

        public int ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        [DefaultValue(true)] public bool Active { get; set; }

        public int? OwnerId { get; set; }

        public DateTime DateCreated { get; set; }
        public virtual ApplicationUser Owner { get; set; }
        public virtual ICollection<Enrollment> Enrollments { get; set; }
        public virtual ICollection<JobEnrollment> JobEnrollments { get; set; }
	    public virtual ICollection<Trainer> Trainers { get; set; }

#region Specifications

	  //  public static Specification<Group> ById(int id) {
			//return new GroupById(id);
	  //  }
	  //  public static Specification<Group> NameContains(string substring) {
			//return new GroupNameContains(substring);
	  //  }
	  //  public static Specification<Group> IsActive(bool value) {
			//return new GroupIsActive(value);
	  //  }

#endregion

	}
}