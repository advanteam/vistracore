﻿using System;

namespace VMS.Infrastructure.DbModel.Models.Training {
	public enum TrainingEventTypes {
		OperationCompletedCorrectly = 0,
		NotTrained = 1,
		SelectedWrongPart = 2,
		SelectedWrongTool = 3,
		PositionedActorWrong = 4,
		ExceededTimeLimit = 5,
		ExceededNumberOfErrors = 6,
		FailedCriticalOperation = 7
	}

	[Serializable]
	public class TrainingEventDTO {
		/// <summary>
		/// The event type that occurred
		/// </summary>
		public TrainingEventTypes EventType { get; set; }

		/// <summary>
		/// The UTC Timestamp for when this event occurred on the client.
		/// </summary>
		public DateTime TimeStamp { get; set; }

		/// <summary>
		/// The Id of the operation that was being performed when the event happened. 
		/// This can be -1 in which case the event does not reference a specific operation.
		/// </summary>
		public int OperationID { get; set; }

		/// <summary>
		/// Was the operation that was performed critical at the time the event was recorded.
		/// If there is no related operation this will always be false.
		/// </summary>
		public bool CriticalOperation { get; set; }

		/// <summary>
		/// This will be the actor ID of the geometry that the operation required. 
		/// If there is no related operation this will be -1
		/// </summary>
		public int ActualGeometry { get; set; }

		/// <summary>
		/// This will be the actor ID of the geometry that the user selected.
		/// If there is no related operation this will be -1
		/// </summary>
		public int SelectedGeometry { get; set; }

		/// <summary>
		/// TrainingSessionID got from server during StartTrainingSession call 
		/// and saved in session.serverSessionId property.
		/// It should be sent with TrainingEventDTO to determine TrainingSession  
		/// </summary>
		public int TrainingSessionID { get; set; }
	}
}