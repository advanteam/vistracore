using System;
using System.ComponentModel;
using VMS.Infrastructure.DbModel.Models.Users;

namespace VMS.Infrastructure.DbModel.Models.Training {
    public class Qualification {
        public Qualification() {
            Active = true;
        }

        public int ID { get; set; }
        public int ApplicationUserID { get; set; }
        public int JobID { get; set; }

        public string Name { get; set; }

        [DefaultValue(true)] public bool Active { get; set; }

        public DateTime Starts { get; set; }
        public DateTime Expires { get; set; }

		public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Job.Job Job { get; set; }
    }
}