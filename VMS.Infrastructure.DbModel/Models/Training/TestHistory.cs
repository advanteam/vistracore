using System;
using VMS.Infrastructure.DbModel.DTO;
using VMS.Infrastructure.DbModel.Models.Job;
using VMS.Infrastructure.DbModel.Models.Users;

namespace VMS.Infrastructure.DbModel.Models.Training {
    public class TestHistory {
        public int ID { get; set; }
        public int ApplicationUserID { get; set; }
        public int JobID { get; set; }
	    public int? OperatorSequenceID { get; set; }
	    public TestResultEnum Result { get; set; }
        public DateTime? TestDate { get; set; }
        public string FailedReason { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Job.Job Job { get; set; }
	    public virtual OperatorSequence OperatorSequence { get; set; }
	}
}