using System;
using System.Collections.Generic;
using VMS.Infrastructure.DbModel.DTO;

namespace VMS.Infrastructure.DbModel.Models.Training {
	public class TrainingSession {
		public int ID { get; set; }
		public int SequenceTrainingID { get; set; }
		public int? TestHistoryID { get; set; }
		public DateTime TimeStarted { get; set; }
		public DateTime TimeEnded { get; set; }
		public DifficultyLevel DifficultyLevel { get; set; }
		public virtual SequenceTraining SequenceTraining { get; set; }
		public virtual TestHistory TestHistory { get; set; }
		public ICollection<TrainingEvent> TrainingEvents { get; set; }
	}
}