using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using VMS.Infrastructure.DbModel.DTO;
using VMS.Infrastructure.DbModel.Models.Job;
using VMS.Infrastructure.DbModel.Models.Users;

namespace VMS.Infrastructure.DbModel.Models.Training {
	public class SequenceTraining {
		public int ID { get; set; }
		public int ApplicationUserID { get; set; }
		public int OperatorSequenceID { get; set; }
		public int TemplateID { get; set; }
		public int? TestHistoryID { get; set; }

		public int Repetitions { get; set; }
		public bool Passed { get; set; }
		public int TimeSpent { get; set; }
		public DateTime? LastTrained { get; set; }
		public TestResultEnum Status { get; set; }

		public virtual ApplicationUser ApplicationUser { get; set; }
		public virtual OperatorSequence OperatorSequence { get; set; }
		public virtual Template Template { get; set; }
		public virtual TestHistory TestHistory { get; set; }
		[InverseProperty("SequenceTraining")]
		public virtual ICollection<TrainingSession> TrainingSessions { get; set; }
	}
}