using System;
using System.Collections.Generic;
using VMS.Infrastructure.DbModel.DTO;

namespace VMS.Infrastructure.DbModel.Models.Training {
    public class Template {
        public int ID { get; set; }
        public string Name { get; set; }
        public string DifficultySequence { get; set; }
        public int Repetitions { get; set; }
        public bool Sealed { get; set; }

        internal IList<DifficultyLevel> GetDifficulties() {
            if (string.IsNullOrEmpty(DifficultySequence)) {
                throw new FormatException("AtLeastOneDifficultyLevel");
            }

            var result = new List<DifficultyLevel>();
            string[] levels = this.DifficultySequence.Trim().Split(',');
            foreach (string item in levels) {
                DifficultyLevel level;
                string parseItem = item.Trim();
                try {
                    level = (DifficultyLevel)Enum.Parse(typeof(DifficultyLevel), parseItem);
                    if (level == DifficultyLevel.None || level == DifficultyLevel.Test) {
                        throw new Exception("Invalid");
                    }
                }
                catch {
                    throw new FormatException(string.Format("NotValidDifficultyLevel {0}", parseItem));
                }

                result.Add(level);
            }

            return result;
        }

        internal void SetDifficulties(IList<DifficultyLevel> difficulties) {
            if (difficulties.Count == 0) {
                throw new FormatException("AtLeastOneDifficultyLevel");
            }

            var i = 0;
            var items = new string[difficulties.Count];
            foreach (DifficultyLevel level in difficulties) {
                if (level == DifficultyLevel.None || level == DifficultyLevel.Test) {
                    throw new FormatException(string.Format((string) "NotValidDifficultyLevelTemplate {0}", (object) level));
                }

                items[i++] = level.ToString();
            }

            this.DifficultySequence = string.Join(",", items);
        }
    }
}