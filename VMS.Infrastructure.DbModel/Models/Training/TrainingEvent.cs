using System;

namespace VMS.Infrastructure.DbModel.Models.Training {
	public class TrainingEvent {
		public int ID { get; set; }
		public int TrainingSessionID { get; set; }
		public TrainingEventTypes EventType { get; set; }
		public DateTime TimeStamp { get; set; }
		public int OperationID { get; set; }
		public bool CriticalOperation { get; set; }
		public int ActualGeometry { get; set; }
		public int SelectedGeometry { get; set; }
		public virtual TrainingSession TrainingSession { get; set; }
	}
}