using System;
using VMS.Infrastructure.DbModel.DTO;

namespace VMS.Infrastructure.DbModel.Models.Analytics {
	public class FilterPreset {
		public int ID { get; set; }
		public FilterPresetEnum Type { get; set; }
		public string Payload { get; set; }
		public string Name { get; set; }
		public string Hash { get; set; }
		public string ReportID { get; set; }
		public int Owner { get; set; }
		public DateTime DateCreated { get; set; }
	}
}