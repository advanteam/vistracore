﻿namespace VMS.Infrastructure.DbModel.Models.Identity {
    public enum LoginStatus {
        Success = 0,
        Failure = 1,
        RequiresVerification = 2,
        LockedOut = 3,
        NoPermission = 4
    }
}