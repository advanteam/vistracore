﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using VMS.Infrastructure.DbModel.Models.Users;

namespace VMS.Infrastructure.DbModel.Models.Identity {

    public enum CreateUserResult {
        Success,
        UsernameExists,
        Error
    }
	
	/// !!!!!!!!!!!!!!
	// TODO - this is very ugly class with a lot of functionality that should not be there (Qualifications, etc). Must be refactored, so only identity code left 
	/// !!!!!!!!!!!!!!
	
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public partial class ApplicationUserManager : UserManager<ApplicationUser> {
        public const int MIN_PASSWORD_LENGTH = 3;

		public ApplicationUserManager(Microsoft.AspNetCore.Identity.IUserStore<ApplicationUser> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<ApplicationUser> passwordHasher, IEnumerable<IUserValidator<ApplicationUser>> userValidators, IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<Microsoft.AspNetCore.Identity.UserManager<ApplicationUser>> logger) 
			: base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger) { }

		//public ApplicationUserManager(DbContext db) : this((IUserStore<ApplicationUser, int>)new ApplicationUserStore(db)) { }

		//public ApplicationUserManager(IUserStore<ApplicationUser, int> store)
		//	: base(store) { }


		public override async Task<bool> CheckPasswordAsync(ApplicationUser user, string password) {
            if (!user.Active) {
                return false;
            }

            //if (user.FirstRole.Value == UserRoles.Trainee) {
            //    return false;
            //}

            return await base.CheckPasswordAsync(user, password);
        }

        // This is basic authorization method that takes login:password format
        public async Task<ApplicationUser> GetApiUser(string sid) {
            string[] split = sid.Split(':');
            if (split.Length != 2) {
                return null;
            }
            string userName = split[0];
            string password = split[1];

            return await this.AuthentificateApiUser(userName, password);
        }

        public async Task<ApplicationUser> AuthentificateApiUser(string name, string password) {
            ApplicationUser user = await this.FindByNameAsync(name);
            if (user == null) {
                return null;
            }

            return !this.CheckPasswordWebApi(user, password) ? null : user;
        }

        protected bool CheckPasswordWebApi(ApplicationUser user, string password) {
            if (!user.Active) {
                return false;
            }

            Task<bool> result = base.CheckPasswordAsync(user, password);
            result.Wait();
            return result.Result;
        }

        //protected void Configure() {
        //    this.UserValidator = new UserValidator<ApplicationUser, int>(this) {
        //        AllowOnlyAlphanumericUserNames = false,
        //        RequireUniqueEmail = false
        //    };

        //    // Configure validation logic for passwords
        //    this.PasswordValidator = new PasswordValidator<> {
        //        RequiredLength = MIN_PASSWORD_LENGTH,
        //        RequireNonLetterOrDigit = false,
        //        RequireDigit = false,
        //        RequireLowercase = false,
        //        RequireUppercase = false,
        //    };

        //    // Configure user lockout defaults
        //    this.UserLockoutEnabledByDefault = true;
        //    this.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
        //    this.MaxFailedAccessAttemptsBeforeLockout = 5;

        //    // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
        //    // You can write your own provider and plug it in here.
        //    this.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<ApplicationUser, int> {
        //        MessageFormat = "Your security code is {0}"
        //    });
        //    this.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser, int> {
        //        Subject = "Security Code",
        //        BodyFormat = "Your security code is {0}"
        //    });
            
        //}

        public string GenerateUserName(string firstName, string lastName) {
            var rnd = new Random();
            return (firstName.Substring(0, 1) + lastName.Substring(0, (lastName.Length > 3 ? 3 : lastName.Length)) + rnd.Next(0, 9) + rnd.Next(0, 9) + rnd.Next(0, 9)).ToLower();
        }

		public static ApplicationUserManager Create() {
			throw new NotImplementedException();
		}

		
	}

}