﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VMS.Infrastructure.DbModel.Models.Users;

namespace VMS.Infrastructure.DbModel.Models.Identity {
    public class ApplicationUserLogin : IdentityUserLogin<int> { }

    public class ApplicationUserClaim : IdentityUserClaim<int> { }

    public class ApplicationUserRole : IdentityUserRole<int> {
        public virtual ApplicationRole Role { get; set; }
    }

	public class ApplicationUserRoleClaim : IdentityRoleClaim<int> { }

	public class ApplicationUserToken : IdentityUserToken<int> { }

    public class ApplicationRole : IdentityRole<int> {
        public ApplicationRole() { }

        public ApplicationRole(string name)
            : this() {
            this.Name = name;
        }

        public UserRoles Value {
            get { return (UserRoles)Enum.Parse(typeof(UserRoles), Name); }
        }
    }

    public class ApplicationUserStore : UserStore{//UserStore<ApplicationUser, ApplicationRole, int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim> {
        //public ApplicationUserStore() : this(new IdentityDbContext()) {
        //    DisposeContext = true;
        //}

        public ApplicationUserStore(DbContext context)
            : base(context) { }
    }


    //public class ApplicationRoleStore : RoleStore<ApplicationRole> {//, int, ApplicationUserRole> {
    //    //public ApplicationRoleStore() : base(new IdentityDbContext()) {
    //    //    DisposeContext = true;
    //    //}

    //    public ApplicationRoleStore(DbContext context) : base(context) { }
    //}
}