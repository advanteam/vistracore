﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace VMS.Infrastructure.DbModel.Models.Identity {
    public class ApplicationRoleManager : RoleManager<ApplicationRole> {
	    public ApplicationRoleManager(IRoleStore<ApplicationRole> store, IEnumerable<IRoleValidator<ApplicationRole>> roleValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, ILogger<RoleManager<ApplicationRole>> logger) 
		    : base(store, roleValidators, keyNormalizer, errors, logger) { }
		//public ApplicationRoleManager(DbContext db)  {}
		//public ApplicationRoleManager(IRoleStore<ApplicationRole> roleStore)
		//    : base(roleStore) { }

		public async Task CreateRoleAsync(string roleName) {
            ApplicationRole role = await this.FindByNameAsync(roleName);
            if (role != null) {
                return;
            }

	        await this.CreateRoleAsync(roleName);
        }

	   
    }
}