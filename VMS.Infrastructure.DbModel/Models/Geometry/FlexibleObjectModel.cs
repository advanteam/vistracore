﻿
namespace VMS.Infrastructure.DbModel.Models.Geometry {
	public class FlexibleObjectModel {
        public int ID { get; set; }
        public int JobID { get; set; }
		public int InstanceID { get; set; } // ID derived from the model
		public int? RootID { get; set; } // Link to the root

		public byte[] Transform { get; set; }
		public bool Fixed { get; set; }

        public int? GeometryInstanceModelID { get; set; }

		public virtual Job.Job Job { get; set; }
		public virtual FlexibleObjectModel Root { get; set; }
		public virtual GeometryInstanceModel GeometryInstanceModel { get; set; }
	}
}
