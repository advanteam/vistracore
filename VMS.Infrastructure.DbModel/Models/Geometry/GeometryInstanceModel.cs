﻿using System.Collections.Generic;

namespace VMS.Infrastructure.DbModel.Models.Geometry {
    public class GeometryInstanceModel {
        public int ID { get; set; }
        public int JobID { get; set; }

        public int GeometryDefinitionID { get; set; }
        public int InstanceID { get; set; } // ID derived from the model

        public byte[] Transform { get; set; }
        public byte[] Tree { get; set; }
        public bool Composite { get; set; }

        public virtual Job.Job Job { get; set; }
        public virtual GeometryDefinition GeometryDefinition { get; set; }
        public virtual ICollection<GeometryInstanceReferenceModel> SecondaryGeometryInstanceReferenceModels { get; set; } // References to children
        public virtual ICollection<GeometryInstanceReferenceModel> PrimaryGeometryInstanceReferenceModels { get; set; } // References to parent
    }
}