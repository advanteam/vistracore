﻿using VMS.Infrastructure.DbModel.Models.Job;

namespace VMS.Infrastructure.DbModel.Models.Geometry {
    public class WorkAreaGeometryConnection : InstanceGeometryConnection {
        public int WorkAreaID { get; set; }
        public virtual WorkArea WorkArea { get; set; }
    }
}