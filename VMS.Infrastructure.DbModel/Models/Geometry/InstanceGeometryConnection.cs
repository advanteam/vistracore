﻿namespace VMS.Infrastructure.DbModel.Models.Geometry {
    public abstract class InstanceGeometryConnection {
        public int ID { get; set; }

        public int GeometryInstanceModelID { get; set; }

        public virtual GeometryInstanceModel GeometryInstanceModel { get; set; }
    }
}