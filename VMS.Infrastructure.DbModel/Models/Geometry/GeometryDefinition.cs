﻿using System.ComponentModel.DataAnnotations;
using VMS.Infrastructure.DbModel.DTO;

namespace VMS.Infrastructure.DbModel.Models.Geometry
{
    public class GeometryDefinition
    {
        public int ID { get; set; }

        [Required]
        public string UID { get; set; }

        [Required]
        public string Name { get; set; }
                
        public GeometryType GeometryType { get; set; }
        
        public float ModelSize { get; set; }

        public byte[] Tree { get; set; }
        
    }
}