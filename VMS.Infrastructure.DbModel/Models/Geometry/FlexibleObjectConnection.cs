﻿namespace VMS.Infrastructure.DbModel.Models.Geometry {
	public class FlexibleObjectConnection {
		public int ID { get; set; }
		public int FromID { get; set; }
		public int ToID { get; set; }

		public virtual FlexibleObjectModel From { get; set; }
		public virtual FlexibleObjectModel To { get; set; }
	}
}