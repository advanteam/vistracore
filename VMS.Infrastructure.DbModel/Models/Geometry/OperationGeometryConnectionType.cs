﻿namespace VMS.Infrastructure.DbModel.Models.Geometry {
	public enum OperationGeometryConnectionType {
		Normal = 0,  // Actor or target
		RigidTarget, // Flex operation
	}
}