﻿using System.ComponentModel.DataAnnotations;
using VMS.Infrastructure.DbModel.Models.Job;

namespace VMS.Infrastructure.DbModel.Models.Geometry {
    public class CameraPosition {
        public int ID { get; set; }
        public int OperationEventID { get; set; }

        public int Order { get; set; }

        [Required] public float X { get; set; }
        [Required] public float Y { get; set; }
        [Required] public float Z { get; set; }

        [Required] public float FieldOfView { get; set; }

        [Required] public bool Selected { get; set; }

        public virtual OperationEvent OperationEvent { get; set; }

        public override string ToString() {
            return string.Format("{0:0.000}, {1:0.000}, {2:0.000}", this.X, this.Y, this.Z);
        }
    }
}