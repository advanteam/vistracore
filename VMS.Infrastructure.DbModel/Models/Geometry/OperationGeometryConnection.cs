﻿using VMS.Infrastructure.DbModel.Models.Job;

namespace VMS.Infrastructure.DbModel.Models.Geometry {
    // This structure connects operation with geometry.
    public class OperationGeometryConnection : InstanceGeometryConnection {
        public int OperationEventID { get; set; }
        public OperationGeometryConnectionType Type { get; set; }
        public virtual OperationEvent OperationEvent { get; set; }
    }
}