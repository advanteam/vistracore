﻿namespace VMS.Infrastructure.DbModel.Models.Geometry {
    public class GeometryInstanceReferenceModel {
        public int ID { get; set; }
        public int ParentModelID { get; set; }
        public int ChildModelID { get; set; }

        public byte[] Transform { get; set; }
        public bool PersistentTooling { get; set; }
        public virtual GeometryInstanceModel ParentModel { get; set; }
        public virtual GeometryInstanceModel ChildModel { get; set; }
    }
}