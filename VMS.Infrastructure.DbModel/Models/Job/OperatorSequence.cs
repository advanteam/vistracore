﻿using System.ComponentModel.DataAnnotations;

namespace VMS.Infrastructure.DbModel.Models.Job {
    public class OperatorSequence {
        public int ID { get; set; }
        public int JobID { get; set; }

        [Required] public string Name { get; set; }

        [Required] [Range(1, 100)] public int Track { get; set; }

        [Required] [Range(0, 100)] public int Variation { get; set; }

        public virtual Job Job { get; set; }
    }
}