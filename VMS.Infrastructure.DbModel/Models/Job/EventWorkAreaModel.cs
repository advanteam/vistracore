﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VMS.Infrastructure.DbModel.DTO;

namespace VMS.Infrastructure.DbModel.Models.Job {
    public class EventWorkAreaModel {
        [Key]
        [ForeignKey("RelatedEvent")]
        public int EventID { get; set; }

        // The current work area of the operation
        public WorkAreaNames WorkArea { get; set; }

        // Used for some of the operations - like composite creation
        public WorkAreaNames FromWorkArea { get; set; }

        // Force start new work area even if the work area for this operation is the same as previous one in the sequence
        public bool ForceSwitchWorkArea { get; set; }

        public bool PersistentTooling { get; set; }

        [Required]
        public virtual Event RelatedEvent { get; set; }
    }
}