﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VMS.Infrastructure.DbModel.Models.Job {
    public class Event {
        public int ID { get; set; }
        public int JobID { get; set; }
        public int? EventGroupID { get; set; }

        public int InstanceID { get; set; }

        public string UID { get; set; }

        [Required] public string Name { get; set; }
        public string Description { get; set; }
        
        public bool UserEvent { get; set; }

        public int SequenceNumber { get; set; }
        
        public virtual Job Job { get; set; }
        public virtual EventGroup EventGroup { get; set; }
        public virtual ICollection<OperatorSequenceEventEnrollment> OperatorSequenceEventEnrollments { get; set; }
        public virtual EventWorkAreaModel EventWorkAreaModel { get; set; }
    }
}