﻿using System.Collections.Generic;
using VMS.Infrastructure.DbModel.Models.Geometry;

namespace VMS.Infrastructure.DbModel.Models.Job {
    public class WorkArea {
        public int ID { get; set; }
        public int JobID { get; set; }
        public string Name { get; set; }

        public virtual Job Job { get; set; }
        public virtual ICollection<WorkAreaGeometryConnection> WorkAreaGeometryConnections { get; set; }
    }
}