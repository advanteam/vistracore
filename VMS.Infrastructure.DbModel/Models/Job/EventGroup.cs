﻿using System.Collections.Generic;

namespace VMS.Infrastructure.DbModel.Models.Job {
    public class EventGroup {
        public int ID { get; set; }
        public int JobID { get; set; }
        public string Name { get; set; }
        public int SequenceNumber { get; set; }

        public virtual Job Job { get; set; }
        public virtual ICollection<Event> Events { get; set; }
    }
}