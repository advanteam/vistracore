﻿using System.Collections.Generic;
using VMS.Infrastructure.DbModel.DTO;
using VMS.Infrastructure.DbModel.Models.Geometry;

namespace VMS.Infrastructure.DbModel.Models.Job {
    public class OperationEvent : Event {
        public OperationType OperationType { get; set; }
        public byte[] ActorTransform { get; set; }
        public byte[] TargetTransform { get; set; }
        public byte[] OffsetTransform { get; set; }

        public bool Critical { get; set; }

        public int? FlexibleObjectModelID { get; set; } // This is a link to the target
        
        public virtual FlexibleObjectModel FlexibleObjectModel { get; set; }
        public virtual ICollection<CameraPosition> CameraPositions { get; set; }
        public virtual ICollection<KeyPointModel> KeyPointModels { get; set; }
        public virtual ICollection<OperationGeometryConnection> OperationGeometryConnections { get; set; }
    }
}