using System.Collections.Generic;
using System.ComponentModel;
using VMS.Infrastructure.DbModel.Models.Training;
using VMS.Infrastructure.DbModel.Models.Users;

namespace VMS.Infrastructure.DbModel.Models.Job {
    public class JobEnrollment {
        public int ID { get; set; }
        public int JobID { get; set; }
        public int GroupID { get; set; }

        public int? TemplateID { get; set; }

        public bool TestRequired { get; set; }
        public int TestSeconds { get; set; }
        public int TestErrors { get; set; }

        public bool OverrideCriticalOperations { get; set; }
	    public bool ObservationMode { get; set; }

	    public virtual Job Job { get; set; }
        public virtual Group Group { get; set; }
        public virtual Template Template { get; set; }

        [DisplayName("Overriden critical operations")] public virtual ICollection<JobEnrollmentCriticalOperation> JobEnrollmentCriticalOperations { get; set; }

	    #region Specifications

	 //   public static Expression<Func<JobEnrollment, bool>> ForJob(Specification<Job> jobSpec) {
		//	return Spec.MemberSpec((JobEnrollment e) => e.Job, jobSpec);
		//}

		#endregion
	}
}