using System;
using VMS.Infrastructure.DbModel.DTO;
using VMS.Infrastructure.DbModel.Models.Users;

namespace VMS.Infrastructure.DbModel.Models.Job {
	public class JobVersion {
		public int ID { get; set; }
		public int JobID { get; set; }
		public int ParentJobID { get; set; }
		public int ApplicationUserID { get; set; }
		public DateTime ActionDate { get; set; }
		public ActionTypeEnum ActionType { get; set; }
		public string Comment { get; set; }
		public virtual Job Job { get; set; }
		public virtual ApplicationUser User { get; set;}
	}
}