using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LinqSpecs.NetStandard;
using VMS.Infrastructure.DbModel.Models.Users;

namespace VMS.Infrastructure.DbModel.Models.Job {
    public class Job {
        public Job() {
            Active = true;
        }
        
        public int ID { get; set; }

        /// <summary>
        /// Unique id of the job (generated)
        /// </summary>
        public string UID { get; set; }

        /// <summary>
        /// Used to identify job by original name from the data source, even if user changes the name
        /// This ID is not always reliable
        /// </summary>
        public string PLMID { get; set; }

        public string Checksum { get; set; }
        
        [Required] public string Name { get; set; }
        
        public DateTime DateCreated { get; set; }

        public string Description { get; set; }

        [DefaultValue(true)] public bool Active { get; set; }

        // TODO might consider making enum of state here, instead of separate booleans

        public bool Preprocessed { get; set; }

        public bool FlexReady { get; set; }

        public float Height { get; set; }

        public int? OwnerId { get; set; }

	    public bool IsVersion { get; set; }

		[NotMapped] public byte[] Content { get; set; }

        // TODO - add intro camera info here

        public virtual ApplicationUser Owner { get; set; }
        public virtual ICollection<JobEnrollment> JobEnrollments { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        public virtual ICollection<OperatorSequence> OperatorSequences { get; set; }
        public virtual ICollection<WorkArea> WorkAreas { get; set; }

	    #region Specifications

	    public static Specification<Job> ById(int id)
	    {
		    return new AdHocSpecification<Job>(job => job.ID == id);
	    }
	    public static Specification<Job> NameContains(string substring)
	    {
		    return new AdHocSpecification<Job>(job => job.Name.Contains(substring));
	    }
	    public static Specification<Job> IsActive(bool value)
	    {
		    return new AdHocSpecification<Job>(job => job.Active);
	    }
	    //public static Specification<Job> ForOwner(Specification<ApplicationUser> userSpec)
	    //{
		   // return Spec.MemberSpec((Job j) => j.Owner, userSpec);
	    //}

		#endregion
	}
}