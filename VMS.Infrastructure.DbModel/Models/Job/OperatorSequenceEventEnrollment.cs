﻿namespace VMS.Infrastructure.DbModel.Models.Job {
    public class OperatorSequenceEventEnrollment {
        public int ID { get; set; }
        public int EventID { get; set; }
        public int OperatorSequenceID { get; set; }
        public virtual Event Event { get; set; }
        public virtual OperatorSequence OperatorSequence { get; set; }
    }
}