﻿using System.Collections.Generic;

namespace VMS.Infrastructure.DbModel.Models.Job {
	public class KeypointHelper {
		public const string DefaultId = "default";
		public const string SpecialCheckId = "specialCheck";
		public const string SpecialInspectId = "specialInspect";

		private static readonly HashSet<string> specialIds;

		static KeypointHelper() {
			specialIds = new HashSet<string> {
				SpecialCheckId,
				SpecialInspectId
			};
		}

		public static bool IsSpecial(string typeId) {
			return specialIds.Contains(typeId);
		}
	}
}