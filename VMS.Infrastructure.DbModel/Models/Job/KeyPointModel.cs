﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace VMS.Infrastructure.DbModel.Models.Job {
    public class KeyPointModel {
        public KeyPointModel() {
            KeyPointType = KeypointHelper.DefaultId;
        }

        public int ID { get; set; }
        public int OperationEventID { get; set; }

        [DisplayName("Key point")] [DataType(DataType.MultilineText)] public string KeyPoint { get; set; }

        [DisplayName("Key point type")] [DefaultValue(KeypointHelper.DefaultId)] public string KeyPointType { get; set; }

        public virtual OperationEvent OperationEvent { get; set; }
    }
}