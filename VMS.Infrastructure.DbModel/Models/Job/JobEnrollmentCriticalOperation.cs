﻿namespace VMS.Infrastructure.DbModel.Models.Job {
    public class JobEnrollmentCriticalOperation {
        public int ID { get; set; }
        public int JobEnrollmentID { get; set; }
        public int OperationEventID { get; set; }

        public virtual JobEnrollment JobEnrollment { get; set; }
        public virtual OperationEvent OperationEvent { get; set; }
    }
}