using System.Diagnostics;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
//using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VMS.Infrastructure.DbModel.Models.Analytics;
using VMS.Infrastructure.DbModel.Models.Geometry;
using VMS.Infrastructure.DbModel.Models.Identity;
using VMS.Infrastructure.DbModel.Models.Job;
using VMS.Infrastructure.DbModel.Models.Training;
using VMS.Infrastructure.DbModel.Models.Users;

namespace VMS.Infrastructure.DbModel.Models {
    public abstract class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int, ApplicationUserClaim, ApplicationUserRole, ApplicationUserLogin, ApplicationUserRoleClaim, ApplicationUserToken> {
	    protected ApplicationDbContext() { }

	    protected ApplicationDbContext(DbContextOptions options)
			: base(options)
		{
		}

	    public DbSet<Group> Groups { get; set; }
        public DbSet<Job.Job> Jobs { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<JobEnrollment> JobEnrollments { get; set; }
        public DbSet<JobEnrollmentCriticalOperation> JobEnrollmentCriticalOperations { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventGroup> EventGroups { get; set; }
        public DbSet<OperatorSequence> OperatorSequences { get; set; }
        public DbSet<OperatorSequenceEventEnrollment> OperatorSequenceEventEnrollments { get; set; }
        public DbSet<GeometryDefinition> GeometryDefenitions { get; set; }
        public DbSet<OperationGeometryConnection> OperationGeometryConnections { get; set; }
        public DbSet<CameraPosition> CameraPositions { get; set; }
        public DbSet<WorkArea> WorkAreas { get; set; }
        public DbSet<WorkAreaGeometryConnection> WorkAreaGeometryConnections { get; set; }
        public DbSet<Qualification> Qualifications { get; set; }
        public DbSet<Template> Templates { get; set; }
        public DbSet<SequenceTraining> SequenceTrainings { get; set; }
        public DbSet<KeyPointModel> KeyPointModels { get; set; }
        public DbSet<GeometryInstanceModel> GeometryInstanceModels { get; set; }
        public DbSet<GeometryInstanceReferenceModel> GeometryInstanceReferenceModels { get; set; }
        public DbSet<UserSetting> UserSettings { get; set; }
        public DbSet<FlexibleObjectModel> FlexibleObjectModels { get; set; }
        public DbSet<FlexibleObjectConnection> FlexibleObjectConnections { get; set; }
        public DbSet<TestHistory> TestHistories { get; set; }
        public DbSet<EventWorkAreaModel> EventWorkAreaModels { get; set; }
		public DbSet<Trainer> Trainers { get; set; }
		public DbSet<JobVersion> JobVersions { get; set; }
		public DbSet<TrainingSession> TrainingSessions { get; set; }
		public DbSet<TrainingEvent> TrainingEvents { get; set; }
	    public DbSet<FilterPreset> FilterPresets { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<GeometryInstanceReferenceModel>()
				.HasOne(m => m.ParentModel)
				.WithMany(t => t.SecondaryGeometryInstanceReferenceModels)
				.HasForeignKey(m => m.ParentModelID)
				.IsRequired();

			modelBuilder.Entity<GeometryInstanceReferenceModel>()
				.HasOne(m => m.ChildModel)
				.WithMany(t => t.PrimaryGeometryInstanceReferenceModels)
				.HasForeignKey(m => m.ChildModelID)
				.IsRequired();

			foreach (var entityType in modelBuilder.Model.GetEntityTypes())
			{
				foreach (var property in entityType.GetProperties())
				{
					foreach (var fk in entityType.FindForeignKeys(property)) {
						if (fk.Relational().Name.Length > 64) {
							fk.Relational().Name = fk.Relational().Name
								.Replace("Geometry", "Geom")
								.Replace("Instance", "Inst")
								.Replace("Reference", "Ref")
								.Replace("Model", "Mdl")
								.Replace("Flexible", "Flex")
								.Replace("Object", "Obj")
								.Replace("Defenition", "Def")
								.Replace("Operation", "Opr")
								.Replace("Connection", "Conn")
								.Replace("WorkArea", "WrkAr")
								.Replace("Enrollment", "Enr")
								.Replace("Critical", "Crtcl")
								.Replace("Operator", "Oprtr")
								.Replace("Sequence", "Seq");
						}						
					}
				}
			}

		}
}
}