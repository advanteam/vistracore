﻿namespace VMS.Infrastructure.DbModel.DTO {
	public enum TestResultEnum {
		None = 0,
		Passed = 1,
		Failed = 2,
		Incomplete = 3,
	}
}