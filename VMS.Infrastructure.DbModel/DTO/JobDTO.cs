using System;
using System.ComponentModel.DataAnnotations;

namespace VMS.Infrastructure.DbModel.DTO {
    public class JobDTO {
        [Required]
        public int ID { get; set; }

        [Required]
        public string UID { get; set; }

        [Required]
        public string PLMID { get; set; }

        [Required]
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        
        public float Height { get; set; }
    }
}