namespace VMS.Infrastructure.DbModel.DTO {
	public enum GroupByEnum {
		None = 0,
		Groups = 1,
		Users = 2,
		Jobs = 3,
		Operations = 4
	}
}