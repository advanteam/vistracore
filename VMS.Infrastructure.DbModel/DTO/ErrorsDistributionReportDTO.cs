using System.Collections.Generic;
using VMS.Infrastructure.DbModel.DTO.Abstraction;

namespace VMS.Infrastructure.DbModel.DTO {
	public class ErrorsDistributionReportDTO {
		public string GroupedBy { get; set; }
		public IEnumerable<IErrorsReport> Items { get; set; }
	}
}