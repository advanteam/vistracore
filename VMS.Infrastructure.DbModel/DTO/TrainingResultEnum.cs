namespace VMS.Infrastructure.DbModel.DTO {
	public enum TrainingResultEnum {
		None = 0,
		NotStarted = 1,
		Pending = 2,
		Qualified = 3,
		TestFailed = 4
	}
}