namespace VMS.Infrastructure.DbModel.DTO {
	public class AvgTimeTrainingsByJobDTO {
		public int JobId { get; set; }
		public string Level { get; set; }
		public int Duration { get; set; }
		public string JobName { get; set; }
	}
}