namespace VMS.Infrastructure.DbModel.DTO {
	public class UserReadinessDTO : ObjectReadinessDTO {
		public string GroupName { get; set; }
		public int GroupId { get; set; }
	}
}