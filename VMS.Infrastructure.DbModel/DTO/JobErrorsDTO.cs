using VMS.Infrastructure.DbModel.DTO.Abstraction;

namespace VMS.Infrastructure.DbModel.DTO
{
	public class JobErrorsDTO : IErrorsReport
	{
		public int ErrorsOfObjectId { get; set; }
		public string Name { get; set; }
		public int TotalErrors { get; set; }
		public int UsersTrained { get; set; }
		public int UsersFailed { get; set; }
		public int Track { get; set; }
	}
}