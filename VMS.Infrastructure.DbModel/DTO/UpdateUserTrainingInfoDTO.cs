namespace VMS.Infrastructure.DbModel.DTO {
	public class UpdateUserTrainingInfoDTO {	 
		public int[] EnableTestSequenceIds { get; set; }
		public int[] ResetProgressSequenceIds { get; set; }
        public int[] RemoveQualificationIds { get; set; }
		public int[] AddQualificationJobIds { get; set; }
	}
}