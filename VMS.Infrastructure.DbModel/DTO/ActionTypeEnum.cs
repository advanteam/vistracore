namespace VMS.Infrastructure.DbModel.DTO {
	public enum ActionTypeEnum {
		None = 0,
		Edit = 1,
		Clone = 2,
		Import = 3
	}
}