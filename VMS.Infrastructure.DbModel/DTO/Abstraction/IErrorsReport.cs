namespace VMS.Infrastructure.DbModel.DTO.Abstraction
{
	public interface IErrorsReport
	{
		int ErrorsOfObjectId { get; set; }
		string Name { get; set; }
		int TotalErrors { get; set; }
		int UsersTrained { get; set; }
		int UsersFailed { get; set; }
	}
}
