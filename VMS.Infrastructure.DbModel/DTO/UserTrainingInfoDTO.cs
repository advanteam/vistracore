using System;

namespace VMS.Infrastructure.DbModel.DTO {
	public class UserTrainingInfoDTO {
		public int ID { get; set; }
		public int GroupID { get; set; }
		public string FullName { get; set; }
		public string UserName { get; set; }
		public TrainingResultEnum Status { get; set; }
		public DateTime? LastTrained { get; set; }
		public int TotalJobs { get; set; }
		public int CompletedJobs { get; set; }
	}
}