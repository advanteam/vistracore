using System;

namespace VMS.Infrastructure.DbModel.DTO {
	public class TrackTrainingInfoDTO {
		public int Track { get; set; }
		public DateTime? LastTrained { get; set; }
		public DateTime? TestDate { get; set; }
		public int SequenceID { get; set; }
		public TestResultEnum Status { get; set; }
		public int Repetitions { get; set; }
	}
}