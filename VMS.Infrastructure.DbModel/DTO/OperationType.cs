﻿namespace VMS.Infrastructure.DbModel.DTO {
	public enum OperationType : int {
		None = 0,
		Interract,
		Move,
		Place,
		Remove,
		PlaceComposite,
		Flex
	}
}