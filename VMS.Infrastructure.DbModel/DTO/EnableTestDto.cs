using System.ComponentModel.DataAnnotations;

namespace VMS.Infrastructure.DbModel.DTO {
	public class EnableTestDto {
		[Required]
		public int SequenceID { get; set; }
	}
}