namespace VMS.Infrastructure.DbModel.DTO {
	public class GroupTrainingInfoDto {
		public int ID { get; set; }
		public string Name { get; set; }
		public int Readiness { get; set; }
		public UserTrainingInfoDTO[] Users { get; set; }
	}
}