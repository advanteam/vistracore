﻿namespace VMS.Infrastructure.DbModel.DTO {
	public enum GeometryType {
		None,
		Part,
		Tool,
		StartConfiguration
	}
}