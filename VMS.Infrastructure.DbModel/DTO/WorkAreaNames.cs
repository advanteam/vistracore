﻿namespace VMS.Infrastructure.DbModel.DTO {
	public enum WorkAreaNames : int {
		FINAL_ASSEMBLY = 0,
		HAND_ASSEMBLY,
	}
}