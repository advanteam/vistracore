using System;

namespace VMS.Infrastructure.DbModel.DTO{
	public class JobVersionDTO {
		public int ID { get; set; }
		public int JobID { get; set; }
		public int ParentJobID { get; set; }
		public int ApplicationUserID { get; set; }
		public DateTime ActionDate { get; set; }
		public ActionTypeEnum ActionType { get; set; }
		public string Comment { get; set; }
		public string UserName { get; set; }
		public string JobName { get; set; }
	}
}