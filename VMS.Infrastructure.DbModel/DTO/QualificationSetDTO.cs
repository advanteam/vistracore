namespace VMS.Infrastructure.DbModel.DTO {
	public class QualificationSetDTO {
		public int JobId { get; set; }
		public int SequenceId { get; set; }
	}
}