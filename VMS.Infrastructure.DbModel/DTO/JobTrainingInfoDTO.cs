using System.Collections.Generic;

namespace VMS.Infrastructure.DbModel.DTO {
	public class JobTrainingInfoDTO {
		public int JobID { get; set; }
		public string JobName { get; set; }
		public bool IsReady { get; set; }
		public bool TestRequired { get; set; }
		public bool Observation { get; set; }
		public int QualificationId { get; set; }
		public int RequiredRepetitions { get; set; }
		public IEnumerable<TrackTrainingInfoDTO> TrackTrainingInfo { get; set; }
	}
}