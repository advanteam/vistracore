namespace VMS.Infrastructure.DbModel.DTO {
	public class ObjectAvgErrorsCountDTO {
		public string Name { get; set; }
		public string GroupByType { get; set; }
		public int ObjectId { get; set; }
		public double AvgErrors { get; set; }
	}
}