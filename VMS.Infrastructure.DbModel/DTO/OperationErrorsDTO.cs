using VMS.Infrastructure.DbModel.DTO.Abstraction;

namespace VMS.Infrastructure.DbModel.DTO {
	public class OperationErrorsDTO : IErrorsReport
	{
		public int ErrorsOfObjectId{ get; set; }
		public int Track { get; set; }
		public int SequenceNumber { get; set; }
		public int GroupSequenceNumber { get; set; }
		public string Name { get; set; }
		public int JobId { get; set; }
		public string JobName { get; set; }
		public int TotalErrors { get; set; }
		public int UsersTrained { get; set; }
		public int UsersFailed { get; set; }
	}
}