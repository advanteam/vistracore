using System;

namespace VMS.Infrastructure.DbModel.DTO {
	public class TestHistoryDTO {
		public int ID { get; set; }
		public int ApplicationUserID { get; set; }
		public int JobID { get; set; }
		public TestResultEnum Result { get; set; }
		public DateTime? TestDate { get; set; }
		public string FailedReason { get; set; }
		public int? OperatorSequenceID { get; set; }
		public int Track { get; set; }
	}
}