﻿namespace VMS.Infrastructure.DbModel.DTO {
	public enum DifficultyLevel {
		None,
		Easy,
		Medium,
		Expert,
		Test
	}
}