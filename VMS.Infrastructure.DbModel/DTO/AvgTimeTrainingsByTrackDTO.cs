namespace VMS.Infrastructure.DbModel.DTO {
	public class AvgTimeTrainingsByTrackDTO {
		public int JobId { get; set; }
		public string JobName { get; set; }
		public int Track { get; set; }
		public string Level { get; set; }
		public int AvgDuration { get; set; }
		public int MaxDuration { get; set; }
		public int MinDuration { get; set; }
		public int SessionsCount { get; set; }
	}
}