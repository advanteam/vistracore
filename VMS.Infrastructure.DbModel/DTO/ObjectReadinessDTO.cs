namespace VMS.Infrastructure.DbModel.DTO {
	public class ObjectReadinessDTO {
		public string Name { get; set; }
		public string GroupByType { get; set; }
		public int ObjectId { get; set; }
		public int Readiness { get; set; }
	}
}