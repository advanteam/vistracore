using System;

namespace VMS.Infrastructure.DbModel.DTO {
	public class GroupDTO {
		public int ID { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public bool Active { get; set; }
		public DateTime DateCreated { get; set; }
	}
}