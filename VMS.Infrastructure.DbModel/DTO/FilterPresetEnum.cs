namespace VMS.Infrastructure.DbModel.DTO {
	public enum FilterPresetEnum {
		None = 0,
		Public = 1,
		Private = 2,
		Factory = 3
	}
}