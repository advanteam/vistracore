namespace VMS.Infrastructure.DbModel.DTO {
	public class UserAvgErrorsCountDTO : ObjectAvgErrorsCountDTO {
		public string GroupName { get; set; }
		public int GroupId { get; set; }
	}
}