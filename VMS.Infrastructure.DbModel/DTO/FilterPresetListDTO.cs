namespace VMS.Infrastructure.DbModel.DTO {
	public class FilterPresetListDTO {
		public int ID { get; set; }
		public FilterPresetEnum Type { get; set; }
		public string Name { get; set; }
		public string Hash { get; set; }
		public string ReportID { get; set; }
	}
}