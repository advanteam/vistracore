﻿using System.Collections.Generic;

namespace VMS.Infrastructure.DbModel.DTO {
	public class UserTestResultDetailsDTO {
		public int ApplicationUserId { get; set; }
		public string UserName { get; set; }
		public int GroupId { get; set; }
		public string GroupName { get; set; }
		public IEnumerable<JobTrainingInfoDTO> JobInfos { get; set; }
	}
}