﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using VMS.Infrastructure.DbModel.Models;

namespace VMS.Infrastructure.DbModel.SqlLite.Models
{
	public class ApplicationDbContextSqliteFactory : IDesignTimeDbContextFactory<ApplicationDbContextSqlite>
	{
		public ApplicationDbContextSqlite CreateDbContext(string[] args)
		{
			var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContextSqlite>();
			optionsBuilder.UseSqlite("Data Source=MyDB.db");//TODO: read connection string from config file
			return new ApplicationDbContextSqlite(optionsBuilder.Options);
		}
	}
}
