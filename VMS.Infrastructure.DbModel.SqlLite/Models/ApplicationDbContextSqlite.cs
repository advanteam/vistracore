using Microsoft.EntityFrameworkCore;
using VMS.Infrastructure.DbModel.Models;

namespace VMS.Infrastructure.DbModel.SqlLite.Models {
    public class ApplicationDbContextSqlite : ApplicationDbContext
    {
		public ApplicationDbContextSqlite(DbContextOptions<ApplicationDbContextSqlite> options)
			: base(options) { }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite("Data Source=MyDB.db");
		}

	    
    }
}