﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VMS.Infrastructure.DbModel.SqlLite.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Sealed = table.Column<bool>(nullable: false),
                    OwnerId = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    VisiblePassword = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_AspNetUsers_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FilterPresets",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Type = table.Column<int>(nullable: false),
                    Payload = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Hash = table.Column<string>(nullable: true),
                    ReportID = table.Column<string>(nullable: true),
                    Owner = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FilterPresets", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "GeometryDefenitions",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UID = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    GeometryType = table.Column<int>(nullable: false),
                    ModelSize = table.Column<float>(nullable: false),
                    Tree = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeometryDefenitions", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Templates",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    DifficultySequence = table.Column<string>(nullable: true),
                    Repetitions = table.Column<int>(nullable: false),
                    Sealed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Templates", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false),
                    RoleId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId1",
                        column: x => x.RoleId1,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    OwnerId = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Groups_AspNetUsers_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Jobs",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UID = table.Column<string>(nullable: true),
                    PLMID = table.Column<string>(nullable: true),
                    Checksum = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Preprocessed = table.Column<bool>(nullable: false),
                    FlexReady = table.Column<bool>(nullable: false),
                    Height = table.Column<float>(nullable: false),
                    OwnerId = table.Column<int>(nullable: true),
                    IsVersion = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Jobs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Jobs_AspNetUsers_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserSettings",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ShowWelcomePage = table.Column<bool>(nullable: false),
                    ApplicationUserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSettings", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserSettings_AspNetUsers_ApplicationUserID",
                        column: x => x.ApplicationUserID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Enrollments",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ApplicationUserID = table.Column<int>(nullable: false),
                    GroupID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enrollments", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Enrollments_AspNetUsers_ApplicationUserID",
                        column: x => x.ApplicationUserID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Enrollments_Groups_GroupID",
                        column: x => x.GroupID,
                        principalTable: "Groups",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Trainers",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ApplicationUserID = table.Column<int>(nullable: false),
                    GroupID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trainers", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Trainers_AspNetUsers_ApplicationUserID",
                        column: x => x.ApplicationUserID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Trainers_Groups_GroupID",
                        column: x => x.GroupID,
                        principalTable: "Groups",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EventGroups",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    JobID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    SequenceNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventGroups", x => x.ID);
                    table.ForeignKey(
                        name: "FK_EventGroups_Jobs_JobID",
                        column: x => x.JobID,
                        principalTable: "Jobs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GeometryInstanceModels",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    JobID = table.Column<int>(nullable: false),
                    GeometryDefinitionID = table.Column<int>(nullable: false),
                    InstanceID = table.Column<int>(nullable: false),
                    Transform = table.Column<byte[]>(nullable: true),
                    Tree = table.Column<byte[]>(nullable: true),
                    Composite = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeometryInstanceModels", x => x.ID);
                    table.ForeignKey(
                        name: "FK_GeomInstMdls_GeomDefs_GeomDefinitionID",
                        column: x => x.GeometryDefinitionID,
                        principalTable: "GeometryDefenitions",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GeometryInstanceModels_Jobs_JobID",
                        column: x => x.JobID,
                        principalTable: "Jobs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JobEnrollments",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    JobID = table.Column<int>(nullable: false),
                    GroupID = table.Column<int>(nullable: false),
                    TemplateID = table.Column<int>(nullable: true),
                    TestRequired = table.Column<bool>(nullable: false),
                    TestSeconds = table.Column<int>(nullable: false),
                    TestErrors = table.Column<int>(nullable: false),
                    OverrideCriticalOperations = table.Column<bool>(nullable: false),
                    ObservationMode = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobEnrollments", x => x.ID);
                    table.ForeignKey(
                        name: "FK_JobEnrollments_Groups_GroupID",
                        column: x => x.GroupID,
                        principalTable: "Groups",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobEnrollments_Jobs_JobID",
                        column: x => x.JobID,
                        principalTable: "Jobs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobEnrollments_Templates_TemplateID",
                        column: x => x.TemplateID,
                        principalTable: "Templates",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "JobVersions",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    JobID = table.Column<int>(nullable: false),
                    ParentJobID = table.Column<int>(nullable: false),
                    ApplicationUserID = table.Column<int>(nullable: false),
                    ActionDate = table.Column<DateTime>(nullable: false),
                    ActionType = table.Column<int>(nullable: false),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobVersions", x => x.ID);
                    table.ForeignKey(
                        name: "FK_JobVersions_AspNetUsers_ApplicationUserID",
                        column: x => x.ApplicationUserID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobVersions_Jobs_JobID",
                        column: x => x.JobID,
                        principalTable: "Jobs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OperatorSequences",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    JobID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Track = table.Column<int>(nullable: false),
                    Variation = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperatorSequences", x => x.ID);
                    table.ForeignKey(
                        name: "FK_OperatorSequences_Jobs_JobID",
                        column: x => x.JobID,
                        principalTable: "Jobs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Qualifications",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ApplicationUserID = table.Column<int>(nullable: false),
                    JobID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Starts = table.Column<DateTime>(nullable: false),
                    Expires = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Qualifications", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Qualifications_AspNetUsers_ApplicationUserID",
                        column: x => x.ApplicationUserID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Qualifications_Jobs_JobID",
                        column: x => x.JobID,
                        principalTable: "Jobs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkAreas",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    JobID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkAreas", x => x.ID);
                    table.ForeignKey(
                        name: "FK_WorkAreas_Jobs_JobID",
                        column: x => x.JobID,
                        principalTable: "Jobs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FlexibleObjectModels",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    JobID = table.Column<int>(nullable: false),
                    InstanceID = table.Column<int>(nullable: false),
                    RootID = table.Column<int>(nullable: true),
                    Transform = table.Column<byte[]>(nullable: true),
                    Fixed = table.Column<bool>(nullable: false),
                    GeometryInstanceModelID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FlexibleObjectModels", x => x.ID);
                    table.ForeignKey(
                        name: "FK_FlexObjMdls_GeomInstMdls_GeomInstMdlID",
                        column: x => x.GeometryInstanceModelID,
                        principalTable: "GeometryInstanceModels",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FlexibleObjectModels_Jobs_JobID",
                        column: x => x.JobID,
                        principalTable: "Jobs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FlexibleObjectModels_FlexibleObjectModels_RootID",
                        column: x => x.RootID,
                        principalTable: "FlexibleObjectModels",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GeometryInstanceReferenceModels",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ParentModelID = table.Column<int>(nullable: false),
                    ChildModelID = table.Column<int>(nullable: false),
                    Transform = table.Column<byte[]>(nullable: true),
                    PersistentTooling = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeometryInstanceReferenceModels", x => x.ID);
                    table.ForeignKey(
                        name: "FK_GeomInstRefMdls_GeomInstMdls_ChildMdlID",
                        column: x => x.ChildModelID,
                        principalTable: "GeometryInstanceModels",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GeomInstRefMdls_GeomInstMdls_ParentMdlID",
                        column: x => x.ParentModelID,
                        principalTable: "GeometryInstanceModels",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TestHistories",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ApplicationUserID = table.Column<int>(nullable: false),
                    JobID = table.Column<int>(nullable: false),
                    OperatorSequenceID = table.Column<int>(nullable: true),
                    Result = table.Column<int>(nullable: false),
                    TestDate = table.Column<DateTime>(nullable: true),
                    FailedReason = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestHistories", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TestHistories_AspNetUsers_ApplicationUserID",
                        column: x => x.ApplicationUserID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TestHistories_Jobs_JobID",
                        column: x => x.JobID,
                        principalTable: "Jobs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TestHistories_OperatorSequences_OperatorSequenceID",
                        column: x => x.OperatorSequenceID,
                        principalTable: "OperatorSequences",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WorkAreaGeometryConnections",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    GeometryInstanceModelID = table.Column<int>(nullable: false),
                    WorkAreaID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkAreaGeometryConnections", x => x.ID);
                    table.ForeignKey(
                        name: "FK_WrkArGeomConns_GeomInstMdls_GeomInstMdlID",
                        column: x => x.GeometryInstanceModelID,
                        principalTable: "GeometryInstanceModels",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WorkAreaGeometryConnections_WorkAreas_WorkAreaID",
                        column: x => x.WorkAreaID,
                        principalTable: "WorkAreas",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Events",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    JobID = table.Column<int>(nullable: false),
                    EventGroupID = table.Column<int>(nullable: true),
                    InstanceID = table.Column<int>(nullable: false),
                    UID = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    UserEvent = table.Column<bool>(nullable: false),
                    SequenceNumber = table.Column<int>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    OperationType = table.Column<int>(nullable: true),
                    ActorTransform = table.Column<byte[]>(nullable: true),
                    TargetTransform = table.Column<byte[]>(nullable: true),
                    OffsetTransform = table.Column<byte[]>(nullable: true),
                    Critical = table.Column<bool>(nullable: true),
                    FlexibleObjectModelID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Events", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Events_EventGroups_EventGroupID",
                        column: x => x.EventGroupID,
                        principalTable: "EventGroups",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Events_Jobs_JobID",
                        column: x => x.JobID,
                        principalTable: "Jobs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Events_FlexibleObjectModels_FlexibleObjectModelID",
                        column: x => x.FlexibleObjectModelID,
                        principalTable: "FlexibleObjectModels",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FlexibleObjectConnections",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FromID = table.Column<int>(nullable: false),
                    ToID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FlexibleObjectConnections", x => x.ID);
                    table.ForeignKey(
                        name: "FK_FlexibleObjectConnections_FlexibleObjectModels_FromID",
                        column: x => x.FromID,
                        principalTable: "FlexibleObjectModels",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FlexibleObjectConnections_FlexibleObjectModels_ToID",
                        column: x => x.ToID,
                        principalTable: "FlexibleObjectModels",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SequenceTrainings",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ApplicationUserID = table.Column<int>(nullable: false),
                    OperatorSequenceID = table.Column<int>(nullable: false),
                    TemplateID = table.Column<int>(nullable: false),
                    TestHistoryID = table.Column<int>(nullable: true),
                    Repetitions = table.Column<int>(nullable: false),
                    Passed = table.Column<bool>(nullable: false),
                    TimeSpent = table.Column<int>(nullable: false),
                    LastTrained = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SequenceTrainings", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SequenceTrainings_AspNetUsers_ApplicationUserID",
                        column: x => x.ApplicationUserID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SequenceTrainings_OperatorSequences_OperatorSequenceID",
                        column: x => x.OperatorSequenceID,
                        principalTable: "OperatorSequences",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SequenceTrainings_Templates_TemplateID",
                        column: x => x.TemplateID,
                        principalTable: "Templates",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SequenceTrainings_TestHistories_TestHistoryID",
                        column: x => x.TestHistoryID,
                        principalTable: "TestHistories",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CameraPositions",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    OperationEventID = table.Column<int>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    X = table.Column<float>(nullable: false),
                    Y = table.Column<float>(nullable: false),
                    Z = table.Column<float>(nullable: false),
                    FieldOfView = table.Column<float>(nullable: false),
                    Selected = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CameraPositions", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CameraPositions_Events_OperationEventID",
                        column: x => x.OperationEventID,
                        principalTable: "Events",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EventWorkAreaModels",
                columns: table => new
                {
                    EventID = table.Column<int>(nullable: false),
                    WorkArea = table.Column<int>(nullable: false),
                    FromWorkArea = table.Column<int>(nullable: false),
                    ForceSwitchWorkArea = table.Column<bool>(nullable: false),
                    PersistentTooling = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventWorkAreaModels", x => x.EventID);
                    table.ForeignKey(
                        name: "FK_EventWorkAreaModels_Events_EventID",
                        column: x => x.EventID,
                        principalTable: "Events",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JobEnrollmentCriticalOperations",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    JobEnrollmentID = table.Column<int>(nullable: false),
                    OperationEventID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobEnrollmentCriticalOperations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_JobEnrCrtclOprs_JobEnrs_JobEnrID",
                        column: x => x.JobEnrollmentID,
                        principalTable: "JobEnrollments",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobEnrollmentCriticalOperations_Events_OperationEventID",
                        column: x => x.OperationEventID,
                        principalTable: "Events",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "KeyPointModels",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    OperationEventID = table.Column<int>(nullable: false),
                    KeyPoint = table.Column<string>(nullable: true),
                    KeyPointType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KeyPointModels", x => x.ID);
                    table.ForeignKey(
                        name: "FK_KeyPointModels_Events_OperationEventID",
                        column: x => x.OperationEventID,
                        principalTable: "Events",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OperationGeometryConnections",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    GeometryInstanceModelID = table.Column<int>(nullable: false),
                    OperationEventID = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperationGeometryConnections", x => x.ID);
                    table.ForeignKey(
                        name: "FK_OprGeomConns_GeomInstMdls_GeomInstMdlID",
                        column: x => x.GeometryInstanceModelID,
                        principalTable: "GeometryInstanceModels",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OperationGeometryConnections_Events_OperationEventID",
                        column: x => x.OperationEventID,
                        principalTable: "Events",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OperatorSequenceEventEnrollments",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    EventID = table.Column<int>(nullable: false),
                    OperatorSequenceID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperatorSequenceEventEnrollments", x => x.ID);
                    table.ForeignKey(
                        name: "FK_OperatorSequenceEventEnrollments_Events_EventID",
                        column: x => x.EventID,
                        principalTable: "Events",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OprtrSeqEventEnrs_OprtrSeqs_OprtrSeqID",
                        column: x => x.OperatorSequenceID,
                        principalTable: "OperatorSequences",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TrainingSessions",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SequenceTrainingID = table.Column<int>(nullable: false),
                    TestHistoryID = table.Column<int>(nullable: true),
                    TimeStarted = table.Column<DateTime>(nullable: false),
                    TimeEnded = table.Column<DateTime>(nullable: false),
                    DifficultyLevel = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainingSessions", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TrainingSessions_SequenceTrainings_SequenceTrainingID",
                        column: x => x.SequenceTrainingID,
                        principalTable: "SequenceTrainings",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TrainingSessions_TestHistories_TestHistoryID",
                        column: x => x.TestHistoryID,
                        principalTable: "TestHistories",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TrainingEvents",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TrainingSessionID = table.Column<int>(nullable: false),
                    EventType = table.Column<int>(nullable: false),
                    TimeStamp = table.Column<DateTime>(nullable: false),
                    OperationID = table.Column<int>(nullable: false),
                    CriticalOperation = table.Column<bool>(nullable: false),
                    ActualGeometry = table.Column<int>(nullable: false),
                    SelectedGeometry = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainingEvents", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TrainingEvents_TrainingSessions_TrainingSessionID",
                        column: x => x.TrainingSessionID,
                        principalTable: "TrainingSessions",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId1",
                table: "AspNetUserRoles",
                column: "RoleId1");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_OwnerId",
                table: "AspNetUsers",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_CameraPositions_OperationEventID",
                table: "CameraPositions",
                column: "OperationEventID");

            migrationBuilder.CreateIndex(
                name: "IX_Enrollments_ApplicationUserID",
                table: "Enrollments",
                column: "ApplicationUserID");

            migrationBuilder.CreateIndex(
                name: "IX_Enrollments_GroupID",
                table: "Enrollments",
                column: "GroupID");

            migrationBuilder.CreateIndex(
                name: "IX_EventGroups_JobID",
                table: "EventGroups",
                column: "JobID");

            migrationBuilder.CreateIndex(
                name: "IX_Events_EventGroupID",
                table: "Events",
                column: "EventGroupID");

            migrationBuilder.CreateIndex(
                name: "IX_Events_JobID",
                table: "Events",
                column: "JobID");

            migrationBuilder.CreateIndex(
                name: "IX_Events_FlexibleObjectModelID",
                table: "Events",
                column: "FlexibleObjectModelID");

            migrationBuilder.CreateIndex(
                name: "IX_FlexibleObjectConnections_FromID",
                table: "FlexibleObjectConnections",
                column: "FromID");

            migrationBuilder.CreateIndex(
                name: "IX_FlexibleObjectConnections_ToID",
                table: "FlexibleObjectConnections",
                column: "ToID");

            migrationBuilder.CreateIndex(
                name: "IX_FlexibleObjectModels_GeometryInstanceModelID",
                table: "FlexibleObjectModels",
                column: "GeometryInstanceModelID");

            migrationBuilder.CreateIndex(
                name: "IX_FlexibleObjectModels_JobID",
                table: "FlexibleObjectModels",
                column: "JobID");

            migrationBuilder.CreateIndex(
                name: "IX_FlexibleObjectModels_RootID",
                table: "FlexibleObjectModels",
                column: "RootID");

            migrationBuilder.CreateIndex(
                name: "IX_GeometryInstanceModels_GeometryDefinitionID",
                table: "GeometryInstanceModels",
                column: "GeometryDefinitionID");

            migrationBuilder.CreateIndex(
                name: "IX_GeometryInstanceModels_JobID",
                table: "GeometryInstanceModels",
                column: "JobID");

            migrationBuilder.CreateIndex(
                name: "IX_GeometryInstanceReferenceModels_ChildModelID",
                table: "GeometryInstanceReferenceModels",
                column: "ChildModelID");

            migrationBuilder.CreateIndex(
                name: "IX_GeometryInstanceReferenceModels_ParentModelID",
                table: "GeometryInstanceReferenceModels",
                column: "ParentModelID");

            migrationBuilder.CreateIndex(
                name: "IX_Groups_OwnerId",
                table: "Groups",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_JobEnrollmentCriticalOperations_JobEnrollmentID",
                table: "JobEnrollmentCriticalOperations",
                column: "JobEnrollmentID");

            migrationBuilder.CreateIndex(
                name: "IX_JobEnrollmentCriticalOperations_OperationEventID",
                table: "JobEnrollmentCriticalOperations",
                column: "OperationEventID");

            migrationBuilder.CreateIndex(
                name: "IX_JobEnrollments_GroupID",
                table: "JobEnrollments",
                column: "GroupID");

            migrationBuilder.CreateIndex(
                name: "IX_JobEnrollments_JobID",
                table: "JobEnrollments",
                column: "JobID");

            migrationBuilder.CreateIndex(
                name: "IX_JobEnrollments_TemplateID",
                table: "JobEnrollments",
                column: "TemplateID");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_OwnerId",
                table: "Jobs",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_JobVersions_ApplicationUserID",
                table: "JobVersions",
                column: "ApplicationUserID");

            migrationBuilder.CreateIndex(
                name: "IX_JobVersions_JobID",
                table: "JobVersions",
                column: "JobID");

            migrationBuilder.CreateIndex(
                name: "IX_KeyPointModels_OperationEventID",
                table: "KeyPointModels",
                column: "OperationEventID");

            migrationBuilder.CreateIndex(
                name: "IX_OperationGeometryConnections_GeometryInstanceModelID",
                table: "OperationGeometryConnections",
                column: "GeometryInstanceModelID");

            migrationBuilder.CreateIndex(
                name: "IX_OperationGeometryConnections_OperationEventID",
                table: "OperationGeometryConnections",
                column: "OperationEventID");

            migrationBuilder.CreateIndex(
                name: "IX_OperatorSequenceEventEnrollments_EventID",
                table: "OperatorSequenceEventEnrollments",
                column: "EventID");

            migrationBuilder.CreateIndex(
                name: "IX_OperatorSequenceEventEnrollments_OperatorSequenceID",
                table: "OperatorSequenceEventEnrollments",
                column: "OperatorSequenceID");

            migrationBuilder.CreateIndex(
                name: "IX_OperatorSequences_JobID",
                table: "OperatorSequences",
                column: "JobID");

            migrationBuilder.CreateIndex(
                name: "IX_Qualifications_ApplicationUserID",
                table: "Qualifications",
                column: "ApplicationUserID");

            migrationBuilder.CreateIndex(
                name: "IX_Qualifications_JobID",
                table: "Qualifications",
                column: "JobID");

            migrationBuilder.CreateIndex(
                name: "IX_SequenceTrainings_ApplicationUserID",
                table: "SequenceTrainings",
                column: "ApplicationUserID");

            migrationBuilder.CreateIndex(
                name: "IX_SequenceTrainings_OperatorSequenceID",
                table: "SequenceTrainings",
                column: "OperatorSequenceID");

            migrationBuilder.CreateIndex(
                name: "IX_SequenceTrainings_TemplateID",
                table: "SequenceTrainings",
                column: "TemplateID");

            migrationBuilder.CreateIndex(
                name: "IX_SequenceTrainings_TestHistoryID",
                table: "SequenceTrainings",
                column: "TestHistoryID");

            migrationBuilder.CreateIndex(
                name: "IX_TestHistories_ApplicationUserID",
                table: "TestHistories",
                column: "ApplicationUserID");

            migrationBuilder.CreateIndex(
                name: "IX_TestHistories_JobID",
                table: "TestHistories",
                column: "JobID");

            migrationBuilder.CreateIndex(
                name: "IX_TestHistories_OperatorSequenceID",
                table: "TestHistories",
                column: "OperatorSequenceID");

            migrationBuilder.CreateIndex(
                name: "IX_Trainers_ApplicationUserID",
                table: "Trainers",
                column: "ApplicationUserID");

            migrationBuilder.CreateIndex(
                name: "IX_Trainers_GroupID",
                table: "Trainers",
                column: "GroupID");

            migrationBuilder.CreateIndex(
                name: "IX_TrainingEvents_TrainingSessionID",
                table: "TrainingEvents",
                column: "TrainingSessionID");

            migrationBuilder.CreateIndex(
                name: "IX_TrainingSessions_SequenceTrainingID",
                table: "TrainingSessions",
                column: "SequenceTrainingID");

            migrationBuilder.CreateIndex(
                name: "IX_TrainingSessions_TestHistoryID",
                table: "TrainingSessions",
                column: "TestHistoryID");

            migrationBuilder.CreateIndex(
                name: "IX_UserSettings_ApplicationUserID",
                table: "UserSettings",
                column: "ApplicationUserID");

            migrationBuilder.CreateIndex(
                name: "IX_WorkAreaGeometryConnections_GeometryInstanceModelID",
                table: "WorkAreaGeometryConnections",
                column: "GeometryInstanceModelID");

            migrationBuilder.CreateIndex(
                name: "IX_WorkAreaGeometryConnections_WorkAreaID",
                table: "WorkAreaGeometryConnections",
                column: "WorkAreaID");

            migrationBuilder.CreateIndex(
                name: "IX_WorkAreas_JobID",
                table: "WorkAreas",
                column: "JobID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "CameraPositions");

            migrationBuilder.DropTable(
                name: "Enrollments");

            migrationBuilder.DropTable(
                name: "EventWorkAreaModels");

            migrationBuilder.DropTable(
                name: "FilterPresets");

            migrationBuilder.DropTable(
                name: "FlexibleObjectConnections");

            migrationBuilder.DropTable(
                name: "GeometryInstanceReferenceModels");

            migrationBuilder.DropTable(
                name: "JobEnrollmentCriticalOperations");

            migrationBuilder.DropTable(
                name: "JobVersions");

            migrationBuilder.DropTable(
                name: "KeyPointModels");

            migrationBuilder.DropTable(
                name: "OperationGeometryConnections");

            migrationBuilder.DropTable(
                name: "OperatorSequenceEventEnrollments");

            migrationBuilder.DropTable(
                name: "Qualifications");

            migrationBuilder.DropTable(
                name: "Trainers");

            migrationBuilder.DropTable(
                name: "TrainingEvents");

            migrationBuilder.DropTable(
                name: "UserSettings");

            migrationBuilder.DropTable(
                name: "WorkAreaGeometryConnections");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "JobEnrollments");

            migrationBuilder.DropTable(
                name: "Events");

            migrationBuilder.DropTable(
                name: "TrainingSessions");

            migrationBuilder.DropTable(
                name: "WorkAreas");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "EventGroups");

            migrationBuilder.DropTable(
                name: "FlexibleObjectModels");

            migrationBuilder.DropTable(
                name: "SequenceTrainings");

            migrationBuilder.DropTable(
                name: "GeometryInstanceModels");

            migrationBuilder.DropTable(
                name: "Templates");

            migrationBuilder.DropTable(
                name: "TestHistories");

            migrationBuilder.DropTable(
                name: "GeometryDefenitions");

            migrationBuilder.DropTable(
                name: "OperatorSequences");

            migrationBuilder.DropTable(
                name: "Jobs");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
