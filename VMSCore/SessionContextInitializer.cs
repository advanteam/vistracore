﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MinDI;

namespace VMSCore {
	public class SessionContextInitializer : ICustomContextInitializer {
		public void Initialize(IDIContext context) {
			// Put any session bindings here. But since we are moving to REST, this will most probably not be used
		}
	}
}