﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MinDI;
using MinDI.Context.Internal;

namespace VMSCore {
	public class VMSContextTypesProvider : IContextBuilderTypesProvider {
		public IList<Type> GetTypes() {
			IList<Type> types = new List<Type>();
			types.Add(typeof(MindiContextInitializer));
			types.Add(typeof(VMSContextInitializer));
			return types;
		}
	}
}
