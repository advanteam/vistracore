﻿using MinDI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VMS.Infrastructure.DbModel.Models;
using VMS.Infrastructure.DbModel.MySql;

namespace VMSCore {
	public class VMSContextInitializer : IApplicationContextInitializer {
		public void Initialize(IDIContext context) {
			// Infrastructure
			//context.s().Bind<ILogMessageDispatcher>(() => new NLogDispatcher());
			//context.s().Rebind<ILog>();
			//context.s().Bind<IErrorProcessingService>(() => new ErrorProcessingService());

			// Legacy
			//context.m().BindGeneric(typeof(ISingleDBFactory<>), typeof(SingleDBFactory<>));

			// Repository
			//context.m().Bind(() => new ApplicationDbContextMySqlFactory().CreateDbContext(new string[0]));
			//context.m().BindGeneric(typeof(IRepository<>), typeof(BaseRepository<>));
			//context.m().Bind<IUnitOfWork>(() => new UnitOfWork());
		}
	}
}
