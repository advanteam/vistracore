﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using MinDI;
using VMS.Infrastructure.DbModel.Models;
using VMS.Infrastructure.DbModel.MySql;

namespace VMSCore
{
	
	public class Startup
	{


		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			//var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>()
			//	.UseMySQL("server=localhost;UserId=root;Password=123456;database=mytestdb;");
			//services.AddSingleton<DbContextOptions<ApplicationDbContext>>(optionsBuilder.Options);

			services.AddDbContext<ApplicationDbContext,ApplicationDbContextMySql>(builder => 
				//builder.UseMySQL("server=localhost;UserId=root;Password=123456;database=mytestdb;"));
				builder.UseSqlite("Data Source=MyDB.db"));

			ContextBuilder.TypesProvider = new VMSContextTypesProvider();
			var diContext = ContextHelper.CreateContext<IGlobalContextInitializer>()
				.Reproduce<IApplicationContextInitializer>();
			services.Configure<CookiePolicyOptions>(options =>
			{
				// This lambda determines whether user consent for non-essential cookies is needed for a given request.
				options.CheckConsentNeeded = httpContext => true;
				options.MinimumSameSitePolicy = SameSiteMode.None;
			});
			services.AddSession(options =>
			{
				options.IdleTimeout = TimeSpan.FromMinutes(10);
			});
			services.AddSingleton<IDIContext>(diContext);
			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

			services.TryAddSingleton<IControllerFactory, VMSControllerFactory>();

			var container = services.BuildServiceProvider();
			diContext.m().Bind<ApplicationDbContext>(() =>
			{
				var scopeFactory = container.GetRequiredService<IServiceScopeFactory>();
				var scope = scopeFactory.CreateScope();
				{
					var contextAccessor = scope.ServiceProvider.GetRequiredService<IHttpContextAccessor>();
					return contextAccessor.HttpContext.RequestServices.GetRequiredService<ApplicationDbContext>();
				}
			});	

			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseStaticFiles();
			app.UseCookiePolicy();
			app.UseSession();
			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}/{id?}");
			});

			using (var serviceScope = app.ApplicationServices.CreateScope())
			{
				using (var dbContext = serviceScope.ServiceProvider.GetService<ApplicationDbContext>())
				{
					dbContext.Database.Migrate();
				}
			}

			//app.Run(async (context) =>
			//{
				
				
				
			//});

		}
	}
}
