﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MinDI;
using VMS.Infrastructure.DbModel.Models;
using VMS.Infrastructure.DbModel.Models.Identity;
using VMS.Infrastructure.DbModel.MySql;

namespace VMSCore
{
	public class RequestContextInitializer : ICustomContextInitializer
	{
		public void Initialize(IDIContext context)
		{
			//context.s().Bind(() => new ApplicationUserManager(context.Resolve<ApplicationDbContext>()));
			//context.s().Bind(() => new ApplicationRoleManager(context.Resolve<ApplicationDbContext>()));
			//context.s().Bind(() => new ApplicationSignInManager(context.Resolve<ApplicationUserManager>(), context.Resolve<IOwinContext>().Authentication));
			context.m().Bind(() => new ApplicationDbContextMySqlFactory().CreateDbContext(new string[0]));

			// Making db single in request
			//context.s().Rebind<ApplicationDbContext>();
			//context.s().Rebind<IMapper>();
			//context.s().BindInstance<IMapper>(new Mapper(context.Resolve<IConfigurationProvider>(), type => context.Resolve(type)));
			//context.s().Rebind<ITranslationService>();
		}
	}
}
