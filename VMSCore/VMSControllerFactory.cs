﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using minioc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.Extensions.DependencyInjection;
using MinDI;

namespace VMSCore
{
	public class VMSControllerFactory : DefaultControllerFactory
	{
		private static void InitSessionBindings(IDIContext context, ISession session)
		{
			context.s().BindInstance(session);
			context.Initialize<SessionContextInitializer>();
		}

		public VMSControllerFactory(IControllerActivator controllerActivator, IEnumerable<IControllerPropertyActivator> propertyActivators) 
			: base(controllerActivator, propertyActivators) { }

		public override object CreateController(ControllerContext requestContext)
		{
			var controller =  base.CreateController(requestContext);
			if (controller == null)
			{
				return null;
			}

			IDIContext context = requestContext.HttpContext.RequestServices.GetRequiredService<IDIContext>();
			//InitSessionBindings(context, requestContext.HttpContext.Session);
			context.InjectDependencies(controller);
			return controller;
		}
	}
}
