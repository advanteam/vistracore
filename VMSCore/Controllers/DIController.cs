﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MinDI;
using MinDI.StateObjects;
using VMS.Infrastructure.DbModel.Models;
using VMS.Infrastructure.DbModel.MySql;

namespace VMSCore.Controllers
{
	public partial class DIController : Controller, IDIClosedContext
	{
		[Injection] public ApplicationDbContext db { get; set; }
		private ContextDescriptor _descriptor = new ContextDescriptor();

		[Injection]
		protected IDIContext contextInjection
		{
			set { _descriptor.context = value; }
		}

		void IDIClosedContext.AfterInjection()
		{
			OnInjected();
		}

		void IDIClosedContext.BeforeFactoryDestruction()
		{
			OnDestruction();
		}

		bool IDIClosedContext.IsValid()
		{
			return _descriptor != null;
		}

		void IDIClosedContext.Invalidate()
		{
			this._descriptor = null;
		}

		ContextDescriptor IDIClosedContext.descriptor
		{
			get { return _descriptor; }
		}

		protected virtual void OnInjected() { }

		protected virtual void OnDestruction() { }
	}

}
